\documentclass{beamer}

\usepackage[francais]{babel}

\usepackage[T1]{fontenc}
\usepackage[export]{adjustbox}
\usepackage[utf8]{inputenc}
\usepackage{movie15}
\usepackage[french]{algorithm2e} 

\beamertemplatenavigationsymbolsempty

\setbeamertemplate{footline}[frame number]

\newcommand*\oldmacro{}%
\let\oldmacro\insertshorttitle%
\renewcommand*\insertshorttitle{%
  \oldmacro\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}

\usetheme{Luebeck}

\graphicspath{{diapo/images/}{../data/}{images/}}

\title[CA-ADF]{Segmentation interactive de tumeurs pour la tomographie par émission de positrons grâce à un automate cellulaire et un filtre de diffusion anisotrope}

\author{Arnaud \textsc{Quillent} et Thibault \textsc{Sauron}}

\date{Mardi 11 décembre 2018}

\begin{document}

    \begin{frame}

    	\titlepage

    \end{frame}
    
    \begin{frame}
    	\frametitle{L'imagerie médicale pour la tomographie}
    	
    	\begin{block}{Tomographie par émission de positrons}
    		Technique d'imagerie médicale qui permet de mesurer	en trois dimensions l'activité d'une cellule grâce aux émissions produites par des positrons présents dans un produit radioactif injecté au préalable
    	\end{block}
    	
    	\begin{exampleblock}{Pourquoi c'est intéressant pour détecter des tumeurs ?}
    		Le produit radioactif injecté est choisi pour ses propriétés chimiques. Après un certain temps, il s'accumule en plusieurs points de l'organisme : cerveau, reins, vessie, c\oe{}ur et métastases cancéreuses
    	\end{exampleblock}
    	
    \end{frame}
    
    \begin{frame}
    	\frametitle{Exemple de tomographies}
    	
    	\begin{figure}
    		\includegraphics[scale=0.22,valign=c]{brain.png}
    		\includegraphics[scale=0.5,valign=c]{pancreas.jpg}
    		\caption{Tomographies d'un cerveau et d'un pancréas}
    	\end{figure}
    
    \end{frame}
    
    \begin{frame}
    	\frametitle{La segmentation de tumeurs}
    	
    	\begin{block}{État de l'art}
			\begin{itemize}
			\item Délimitation manuelle
			\item Approches automatisées :
				\begin{itemize}
					\item Fuzzy clustering
					\item Croissance adaptive de région
					\item Contours actifs à double-front
				\end{itemize}
    		\end{itemize}
    	\end{block}
    	
    	\begin{alertblock}{Inconvénients de ces techniques}
    		\begin{itemize}
    			\item Souvent inefficaces sur les cas réels
    			\item Adaptées à seulement certains types de tumeurs :
    				\begin{itemize}
						\item Tumeurs du poumon (généralement de grande taille)
						\item Tumeurs de la tête et du cou (zones avec moins d'organes et de muscles)
    			\end{itemize}
    			\item Pas assez précises sur les tumeurs de petite taille
    		\end{itemize}
    	\end{alertblock}
    	
    \end{frame}
    
    \begin{frame}
    	\frametitle{La segmentation interactive}
    	
    	\begin{block}{Principe}
    		Techniques basées sur l'utilisation d'opérateurs connus et largement utilisés couplés à la reconnaissance de formes
    	\end{block}
    	
    	\begin{exampleblock}{Fonctionnement}
    		L'utilisateur indique à l'ordinateur des régions de départ de taille réduite (\textit{seeds}) correspondant à des zones saines (arrière-plan) et des zones cancéreuses (premier plan). L'algorithme déduit la catégorie des pixels non-définis suivant un critère qui dépend de ces \textit{seeds}
    	\end{exampleblock}
    
    \end{frame}
    
    \begin{frame}
    	\frametitle{CA-ADF}
    	
    	\begin{block}{Principe}
    		Utiliser un filtre de diffusion anisotrope (\textit{ADF}) suivi d'un automate cellulaire (\textit{CA})
    	\end{block}
    	
    	\begin{exampleblock}{Pourquoi ?}
    		\begin{itemize}
				\item L'automate cellulaire simule le comportement d'une cellule organique
				\item Le filtre de diffusion anisotrope réduit le bruit tout en conservant les contours
			\end{itemize}
    	\end{exampleblock}
    	
    \end{frame}
    	
    \begin{frame}
	    \frametitle{La diffusion anisotrope}

	    \begin{block}{Principe}
			On veut réaliser un lissage uniquement sur les parties homogènes de l'image et préserver les contours. Le lissage utilise les equations de diffusion de la chaleur dans un milieu homogène et les applique à une image en fonction de la présence de contours ou non
		\end{block}

    	\begin{exampleblock}{Réalisation}
			On applique le lissage en suivant l'inverse du gradient avec une fonction dite de diffusivité. Une fonction de diffusivité doit être décroissante et valoir 1 en 0. Plus le gradient est fort, plus la présence d'un contour est probable et moins on lisse
    	\end{exampleblock}
		
    \end{frame}

	\begin{frame}
		\frametitle{La diffusion anisotrope}
		
		\begin{block}{Paramètres}
			On peut faire varier trois paramètres pour la diffusion anisotrope:
			\begin{itemize}
				\item le nombre d'itérations
				\item la fonction de diffusivité
				\item le paramètre $\lambda$ de la fonction de diffusivité
			\end{itemize}
			Avec ces paramètres on peut favoriser une plus grande uniformité des zones ou une meilleure préservation des contours
		\end{block}

	\end{frame}


	\begin{frame}
		\frametitle{La fonction de diffusivité}
		
		\begin{figure}
			\includegraphics[scale = 0.66,center]{images/demo_diff.jpg}
		\end{figure}

	\end{frame}

	\begin{frame}
		\frametitle{$\lambda$ = 20}
		
		\begin{center}
			\includegraphics[scale = 0.8,center]{demo_20.jpg}
		\end{center}

	\end{frame}

	\begin{frame}
		\frametitle{$\lambda$ = 70}
		
		\begin{center}
			\includegraphics[scale=0.8,center]{demo_70.jpg}
		\end{center}

	\end{frame}

	\begin{frame}
    	\frametitle{Les automates cellulaires}
    	
    	\begin{block}{Définition}
    		C'est une grille de cellules ayant chacune un état choisi parmi certaines possibilités prédéfinies. L'automate évolue au cours du temps : l'état à \textit{t+1} d'une cellule dépend de l'état au temps \textit{t} d'un nombre de cellules qui lui sont voisines. Chaque nouvelle génération dépend donc de celle qui la précède, et par extension, de l'état initial de l'automate
    	\end{block}
    
    	\begin{columns}[c]
    		\begin{column}{3cm}
				\begin{figure}
					\includemovie[attach=false,autoplay=true,repeat,poster=images/jDLVfirst.jpg]{3cm}{3cm}{images/jeuDeLaVie.gif}
				\end{figure}
			\end{column}
			\begin{column}{7cm}
				\begin{exampleblock}{Le jeu de la vie}
					\begin{itemize}
						\item Morte ayant 3 voisines vivantes \\$\rightarrow$ vivante
						\item Vivante ayant 2 ou 3 voisines vivantes \\	$\rightarrow$ vivante
						\item Sinon $\rightarrow$ morte
					\end{itemize}
				\end{exampleblock}
			\end{column}
		\end{columns}
    
    \end{frame}
    
    \begin{frame}
    	\frametitle{L'automate du CA-ADF}
    	
    	\begin{block}{Définition}
    		Un automate cellulaire peut être défini comme un triplet $A = (S,N,\delta)$ avec :
    		\begin{itemize}
    			\item S : un ensemble d'états
    			\item N : un système de voisinage
    			\item $\delta$ : la règle de l'automate (fonction pour passer de \textit{t} à \textit{t+1})
    		\end{itemize}
    	\end{block}
    	
    	\begin{block}{Cas du CA-ADF}
    		Une cellule correspond à un pixel de l'image. L'état de chaque pixel est constitué d'un label $\ell_p$, d'une force $\theta_p$ et d'une intensité $I_p$.
    		\\ Le système de voisinage utilisé est celui de Moore
   		\end{block}
   		
   		\begin{figure}
   			\includegraphics[scale=0.3]{moore_1.png}
   			\kern 2cm
   			\includegraphics[scale=0.3]{moore_2.png}
		\end{figure}
	\end{frame}
    
    \begin{frame}
    	\frametitle{L'automate du CA-ADF}
    	
    	\begin{block}{Initialisation}
    		Pour chaque pixel $p_{seed}$, on initialise l'état suivant :
    		\\ $\ell_{p_{seed}} =
    		\begin{cases}
    			1, & \mbox{si } p_{seed} \in \mbox{premier plan}
    			\\ 2, & \mbox{si } p_{seed} \in \mbox{arrière-plan}
    		\end{cases} $, 
    		\kern 0.5cm
    		$\theta_{p_{seed}} = 1$
    	\end{block}
    	
    	\begin{columns}
    		\begin{column}{6cm}
				\begin{algorithm}[H]
					\Pour{$\forall_p \in P$}{
					$\ell^{t+1}_{p} = \ell^{t}_{p}$
					\\$\theta^{t+1}_{p} = \theta^{t}_{p}$
						\\\Pour{$\forall_q \in N(p)$}{
							\Si{$g(|I_p - I_q|) \times \theta^{t}_{q} > \theta^{t}_{p}$}{
								$\ell^{t+1}_{p} = \ell^{t}_{q}$
								\\$\theta^{t+1}_{p} = g(|I_p - I_q|) \times \theta^{t}_{q}	$
							}
						}
					}
				\end{algorithm}
			\end{column}
			\begin{column}{4cm}
				\underline{Avec :}\vspace{0.5cm}
				\\
				$g(x) = 1-\frac{w_p \cdot x}{\max{(I)}}$
				\vspace{0.5cm}
				\\
				$w_p = \frac{1}{1+\sqrt{\frac{1}{n}\sum^n_{q=1}(I_p - I_q)^2}}$
			\end{column}
    	\end{columns}
    	
	\end{frame}

	\begin{frame}
		\frametitle{Comparaison}
		\begin{figure}
			\kern 0.5cm
			\includegraphics[scale=0.2,center]{comparaison.png}
		\end{figure}
	\end{frame}

	\begin{frame}
		\frametitle{Références}
		\nocite{*}
		\bibliographystyle{abbrv}
		\bibliography{bibli}
		
	\end{frame}

\end{document}