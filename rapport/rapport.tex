\documentclass{scrartcl}

\usepackage[french]{babel} % Définit la langue du document
\usepackage[utf8]{inputenc} % Définit les caractères accentuées comme ceux de l'UTF-8
\usepackage[T1]{fontenc} % On utilise les fontes accentuées de Latex
\usepackage{graphicx} % Pour pouvoir insérer des images
\usepackage[left=3cm,right=3cm,top=3cm,bottom=3cm]{geometry} % On redéfinit les marges de notre document
\usepackage{float} % Placement cool des images
\usepackage[export]{adjustbox}
\usepackage[french]{algorithm2e}
\usepackage{caption}
\usepackage{amsmath}
\usepackage{listingsutf8}

\lstset{
    frame = single,
    language = Bash,    
    xleftmargin = 0pt,
    xrightmargin = 0pt,
    resetmargins = true,
    inputencoding=utf8/latin1,
    showspaces = false,
    showstringspaces=false,
    breaklines = true,
    literate={á}{{\'a}}1 {ã}{{\~a}}1 {é}{{\'e}}1,
    basicstyle=\footnotesize
}

\makeatletter
\renewcommand\@biblabel[1]{\textbullet}
\makeatother

\renewcommand\refname{}

\captionsetup{justification=centering}

\graphicspath{{images/}{../diapo/images/}{../data/}}

\PassOptionsToPackage{hyphens}{url}\usepackage[hidelinks]{hyperref}

\begin{document}

\title{Projet TIM}
\subtitle{Segmentation interactive de tumeurs pour la tomographie par émission de positrons grâce à un automate cellulaire et un filtre de diffusion anisotrope \\ (CA-ADF)}
\author{Arnaud \textsc{Quillent} \\ Thibault \textsc{Sauron}}
\date{Mardi 11 décembre 2018}
\maketitle

\newpage

\tableofcontents

\newpage

\section{Avant-propos}

    Dans le cadre de l'EC Traitement d'Images d'ASI 4, nous avons étudié un texte de recherche.
    Les chercheurs qui l'ont écrit désiraient améliorer la segmentation de tumeurs sur des images provenant d'une Tomographie par Émission de Positrons (TEP).
    
    La TEP consiste à injecter dans le corps du patient un produit radioactif inoffensif pour la santé. Celui-ci est choisi en fonction de ses propriétés chimiques. Après un certain temps d'absorption, ce produit est concentré dans les reins, le c\oe{}ur, le cerveau, la vessie et les métastases cancéreuses. Ainsi, les cellules correspondant à ces zones apparaissent très clairement sur les images TEP.
    
    Toutefois, les images créées par cette technique d'imagerie médicale sont compliquées à traiter informatiquement. En effet, elles ont une très basse résolution et leur rapport signal sur bruit est très petit.
    
    Pour segmenter ces images, plusieurs techniques sont utilisées : 
    \begin{itemize}
        \item \underline{la délimitation manuelle :} le chercheur détoure lui-même la tumeur. Cette technique est la plus efficace, mais elle est extrêmement chronophage.
        \item \underline{les algorithmes automatisés :} à l'heure où le machine learning devient omniprésent, il s'avère que les techniques automatisées ne peuvent s'appliquer qu'à certains types de tumeurs (de grande taille, ou situées dans des zones peu musclées, générant moins d'interférences). Ces algorithmes sont par exemple incapables de segmenter les petites tumeurs et celles ayant un contraste peu élevé.
    \end{itemize}

L'idée des chercheurs de notre sujet a donc été d'utiliser la segmentation interactive, couplée à deux techniques connues et répandues : le filtre de diffusion anisotrope (\textit{Anisotropic Diffusion Filter, ADF}) et l'automate cellulaire (\textit{Cellular Automaton, CA}).

\begin{figure}[H]
        \includegraphics[scale=0.5,center]{pet_throat.jpg}
        \caption{Exemple d'imagerie TEP présentant une tumeur de l'\oe{}sophage}
\end{figure}

\newpage

\section{La segmentation interactive}

Puisque les techniques algorithmiques entièrement automatisées (c'est-à-dire sans intervention humaine) sont inefficaces dans le cas des images TEP, pourquoi ne pas autoriser qu'un opérateur aide l'algorithme ?
C'est ce que l'on appelle la segmentation interactive.

Cette technique repose sur l'indication de pixels sources (\textit{seeds} en anglais), correspondant à des zones saines et des zones cancéreuses. Ces \textit{seeds} sont de tailles réduites, et sont sélectionnés directement par le chercheur à partir de l'image TEP.
Chaque \textit{seed} est <<labellisé>> suivant s'il correspond à une zone saine, une zone cancéreuse ou une zone non-définie.

L'algorithme est ensuite appliqué aux \textit{seeds}, et celui-ci définit le labels des pixels <<neutres>> en parcourant l'image et en lui appliquant certaines transformations.
\bigbreak
Dans le cas de notre sujet, deux transformations sont effectuées sur l'image. Un filtre de diffusion anisotrope est d'abord appliqué, de façon à augmenter le contraste et diminuer le bruit tout en conservant des contours nets. Puis, on fait tourner un automate cellulaire, de façon à simuler informatiquement le comportement des cellules cancéreuses, qui grossissent et contaminent leurs voisines saines.

\section{La diffusion anisotrope}
    
    \subsection{Présentation}

    La diffusion anisotrope utilise les équations de diffusion de la chaleur dans un corps physique et l'applique aux images : elle uniformise les niveaux de gris. 
    L'équation de la difusion est :
    \[
       \frac{\partial T(x,t)}{\partial  t} = c . \frac{\partial^2 T(x,t)}{\partial x^2}
     \]
    Pour que cette diffusion n'efface pas les contours (anisotrope signifiant non-indépendant de la direction) Pietro Perona et Jitendra Malik ont modifié l'équation pour que les contours ne soient pas diffusés :
    \[
       \frac{\partial L(r,t)}{\partial  t} = div( c(r,t).\nabla L(r,t))
     \]

    Où $c$ est la fonction inverse du gradient. Plus le gradient aux alentours est fort, plus $c$ est petit et moins on diffuse. Perona et Malik proposent deux fonctions pour $c$ :
    \[
    \exp{(-\frac{\nabla I}{ \lambda^2})} \ \ \ et\ \ \ \frac{1}{1+\frac{ || \nabla I ||^2}{\lambda^2}} 
    \]
    Les deux fonctions remplissent un rôle différent : la première conserve mieux les contours, la seconde diffuse plus.

    \subsection{Exemples}

    \begin{figure}[H]
        \includegraphics[center]{demo_diff.jpg}
        \caption{Exemple de diffusion anisotrope}
    \end{figure}

    On voit bien que l'histogramme présente plus de classes après la diffusion. Les contours sont mieux préservés en utilisant la première fonction. Pour la deuxième, seul certains contours très nets sont présevés (en général les changements dans une seule direction du gradient).

    \begin{figure}[H]
        \centering
        \includegraphics{demo_20.jpg}
        \caption{Diffusion anisotrope avec $\lambda = 20$}
    \end{figure}

    \begin{figure}[H]
        \centering
        \includegraphics{demo_70.jpg}
        \caption{Diffusion anisotrope avec $\lambda = 70$}
    \end{figure}

    \bigbreak
    On voit bien que des petites valeurs de $\lambda$ favorisent une plus grande préservation des contours. Un grand nombre d'itérations homogénéise les zones délimitées par des contours, et pour les zones sans transition forte, crée des <<escaliers>> de niveau de gris.

    \subsection{Applications à la tomographie}

    L'application aux images tomographiques est assez simple : ce sont des image de basse résolution, assez bruitées et qui ont des contours facilement identifiables par un humain, mais pas par une machine. La diffusion permet d'augmenter la différence entre deux zones homogènes et surtout ne perturbe pas les angles et positions. Elle renforce uniquement les contours déjà présents et c'est ce qui fait sa force pour l'imagerie médicale.

    \section{L'automate cellulaire}

    \subsection{Présentation}

        Un automate cellulaire est une grille de cellules ayant chacune un état choisi parmi certaines possibilités prédéfinies. L'automate évolue au cours du temps : l'état à \textit{t+1} d'une cellule dépend de l'état au temps \textit{t} d'un nombre de cellules qui lui sont voisines.
        Chaque nouvelle génération dépend donc de celle qui la précède, et par extension, de l'état initial de l'automate.

        \begin{figure}[h]
            \includegraphics[scale=0.5]{jeuVie.png}
            \caption{Exemple d'évolution du Jeu de la Vie, l'un des premiers automates cellulaires créés (1970)}
        \end{figure}
    
    \subsection{Cas du CA-ADF}

        \subsubsection{Structure de l'automate}

        Comme énoncé plus haut, les chercheurs de notre sujet ont utilisé un automate cellulaire pour simuler le comportement de cellules cancéreuses.
        L'automate cellulaire du CA-ADF peut être définit comme suit : $ A = (S,N,\delta) $, avec

        \begin{itemize}
            \item \underline{S :} un ensemble d'états (aussi appelés générations)
            \item \underline{N :} un système de voisinage (qui définit les cellules voisines agissant sur une cellule donnée)
            \item \underline{$\delta$ :} la règle de l'automate (la fonction pour passer de l'état \textit{t} à l'état \textit{t+1})
        \end{itemize}

        \bigbreak

        Chaque pixel de l'image TEP correspond à une cellule. Chacune de ces cellules est constituée :
        
        \begin{itemize}
            \item \underline{d'un label $\ell_p$ :} 0 si cellule neutre, 1 si cellule cancéreuse ou 2 si cellule saine
            \item \underline{d'une force $\theta_p$ :} comprise entre 0 et 1, symbolise la capacité d'une cellule à contaminer ses voisines
            \item \underline{d'une intensité $I_p$ :} correspond au niveau de gris du pixel, et donc à l'intensité de la tumeur (plus le pixel est foncé, plus la cellule est contaminée)
        \end{itemize}

        \bigbreak

        \subsubsection{Les pixels sources}

        L'algorithme décrit par le sujet permet à l'utilisateur de sélectionner des zones sources (\textit{seeds}) dans l'image, correspondant aux cellules cancéreuses ou saines.
        Cependant, un tel dispositif est compliqué à coder, nous avons donc utilisé une autre approche :
        notre programme prend en entrée l'image TEP à traiter ainsi qu'une image \textit{seedée}, c'est-à-dire avec des traits de couleur (rouge pour les zones saines, bleues pour les zones cancéreuses).
        Le programme reconnaît ensuite ces deux couleurs, et traite les pixels correspondant comme des \textit{seeds}.

        \begin{figure}[H]
            \centering
            \includegraphics[scale=2]{tumor_esophagus.jpg}
            \kern 1cm
            \includegraphics[scale=2]{seeds_esophagus.jpg}
            \caption{Exemple d'image TEP de tumeur et de son image \textit{seedée} correspondante}
        \end{figure}

        Pour chaque pixel \textit{seed} $p_{seed}$, on initialise l'état suivant :
        $$ \ell_{p_{seed}} =
        \begin{cases}
            1, & \mbox{si } p_{seed} \in \mbox{premier plan}
            \\ 2, & \mbox{si } p_{seed} \in \mbox{arrière-plan}
            \\ 0, & \mbox{sinon}
        \end{cases}, \kern 1cm \theta_{p_{seed}} = 1, \kern 1cm I_{p_{seed}} = niveau\ de\ gris $$

        \newpage

        \subsubsection{Le système de voisinage}

        On définit le système de voisinage suivant la règle de Moore d'ordre $k$ :
        
        $$ N(p) = \{ q \in Z^n : \|p-q\|_\infty := \max_{i \in [1..n]} |p_i - q_i| = k \} $$

        Dans le cas de l'automate du sujet, on prend $k=1$.

        \begin{center}
            \begin{figure}[H]
                \centering
                \includegraphics[scale=0.6,valign=c]{moore_1.png}
                \kern 1cm
                \includegraphics[scale=0.6,valign=c]{moore_2.png}
                \caption{Voisinages de Moore d'ordre 1 et d'ordre 2 autour du pixel noir}
            \end{figure}
        \end{center}
        
        \bigbreak

         \subsubsection{Règle d'évolution}

        La règle d'évolution $\delta$ suit l'algorithme suivant :
        \\

        \begin{algorithm}[H]
            // pour chaque pixel de l'image
            \\
            \Pour{$\forall_p \in P$}{
            $\ell^{t+1}_{p} = \ell^{t}_{p}$ // on copie le label
            \\$\theta^{t+1}_{p} = \theta^{t}_{p}$ // on copie la force
            \\ // pour chaque pixel du voisinage
                \\\Pour{$\forall_q \in N(p)$}{
                    // si la force diminuée du voisin est supérieure à la force du pixel
                    \\
                    \Si{$g(|I_p - I_q|) \times \theta^{t}_{q} > \theta^{t}_{p}$}{
                        $\ell^{t+1}_{p} = \ell^{t}_{q}$ // le pixel prend le label du voisin
                        \\$\theta^{t+1}_{p} = g(|I_p - I_q|) \times \theta^{t}_{q} $ // le pixel prend la force diminuée du voisin
                    }
                }
            }
        \end{algorithm}
        
        \bigbreak

        La fonction $g$ utilisée dans l'algorithme est monotone décroissante et ses valeurs sont comprises entre 0 et 1.
        Elle est définie comme suit :
        
        $$ g(x) = 1-\frac{x \cdot w_p}{\max{|I|}} $$

        $\max{|I|}$ est la valeur d'intensité maximale de tous les pixels. Ici, nous sommes dans le cas d'images en niveaux de gris, l'intensité maximale est donc de 255.
        \bigbreak
        $w_p$ est une fonction de poids appliquée pour lisser les pixels du voisinage :

        $$ w_p = \frac{1}{1+\sqrt{\frac{1}{n}\sum^n_{q=1}(I_p - I_q)^2}} $$

        On peut remarquer que l'expression dans la racine est la formule de l'erreur moyenne quadratique (\textit{MSE} en anglais pour \textit{Mean Square Error}). Celle-ci est calculée entre l'intensité du pixel traité et celle de chacun de ses voisins.
        \\

        Nous pouvons expliquer ces fonctions mathématiques d'une manière simple : si la différence d'intensité entre le pixel traité et un pixel voisin est grande (c'est à dire que l'un est plus noir que l'autre, donc beaucoup plus contaminé), la capacité du pixel contaminé à attaquer le pixel central (calculée par la fonction $g$) est élevée.
        \\

        \subsection{Analogie biologique}

        On peut facilement voir une analogie entre notre automate et le corps humain :
        le processus entier peut être interprété comme une lutte entre cellules cancéreuses et saines, les premières cherchant à contaminer les secondes, et les secondes cherchant à survivre et détruire les premières.
        Durant les premières générations de l'automate, les cellules \textit{seeds} se multiplient et cherchent à occuper tout l'espace disponible. Dès que les cellules cancéreuses et saines entrent en contact, elles s'attaquent : les cellules sont attaquées chacune leur tour par leurs voisines.
        La capacité d'une cellule à en attaquer une autre dépend de sa force $\theta$ et de son éloignement de la cellule cible (ici, il s'agit de l'éloignement colorimétrique).
        Au fur et à mesure des générations, les cellules les plus fortes se multiplient au détriment de leurs voisines et s'étendent dans tout le corps, et donc dans toute l'image.

\section{Comparaison de CA-ADF avec d'autres méthodes de segmentation}

En comparant leur nouvelle méthode de segmentation avec celles couramment utilisées, les chercheurs de notre sujet ont remarqué qu'il était beaucoup plus performant.

\begin{figure}[H]
    \includegraphics[scale=0.4,center]{compa-sujet.png}
    \caption{Comparaison de CA-ADF avec d'autres méthodes de segmentation\\(image tirée du sujet)}
    \label{comparaison}
\end{figure}

\underline{Légende de la figure \ref{comparaison} :}
\\
\begin{itemize}
    \item CCRG : \textit{Confidence Connected Region Growing}
    \item SRG : \textit{Seeded Region Growing}
\end{itemize}

\bigbreak

Cependant, les algorithmes \textit{CCRG} et \textit{SRG} sont issus de recherches auxquelles nous n'avont pas pu accéder. 
Nous avons donc comparé \textit{CA-ADF} avec le seuillage d'Otsu et la croissance de région de Watershed :

\begin{figure}[H]
    \includegraphics[scale=0.33,center]{comparaison.png}
    \caption{Comparaison effectuée avec nos algorithmes}
\end{figure}

\bigbreak

On remarque bien que \textit{CA-ADF} est plus performant que les autres méthodes de segmentation testées.

\section{Utilisation du programme}
    
    Pour utiliser le programme, il faut se déplacer jusque dans le répertoire du rapport et lancer la commande suivante :

    \begin{lstlisting}[gobble=7]
        python3 src/main.py chemin_image_TEP chemin_image_seedée nbGénérations
    \end{lstlisting}
    
    \kern 0.5cm

    Des exemples d'images à utiliser sont situées dans le dossier \texttt{data} : 
    
    \begin{itemize}
        \item \texttt{tumor\_esophagus.jpg}
        \item \texttt{seeds\_esophagus.jpg}
    \end{itemize}
    ou
    \begin{itemize}
        \item \texttt{tumor\_lung.jpg}
        \item \texttt{seeds\_lung.jpg}
    \end{itemize}

    \kern 0.5cm

    Les premières montrent une tumeur de l'\oe{}sophage, les secondes une tumeur du poumon.

    \begin{figure}[h]
        \centering
        \includegraphics[scale=1.5]{auto0.jpg}
        \includegraphics[scale=1.5]{auto4.jpg}
        \includegraphics[scale=1.5]{auto7.jpg}
        \includegraphics[scale=1.5]{auto10.jpg}
        \includegraphics[scale=1.5]{auto15.jpg}
        \includegraphics[scale=1.5]{auto20.jpg}
        \caption{Exemple d'évolution de CA-ADF}
        \bigbreak
        \includegraphics[scale=0.015]{tumor_lung.jpg}
        \caption{Image TEP originale}
    \end{figure}

\newpage

\section{Sources}

\nocite{*}
\bibliographystyle{abbrv}
\bibliography{../diapo/bibli}

\end{document}