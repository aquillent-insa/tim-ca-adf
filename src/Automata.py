from State import State
import numpy as np
import math
import copy

def weight(p, neigh): # Fonction de poids
    ip = p.getIntensity() * np.zeros((neigh.size,1)) # On crée un vecteur avec toutes les valeurs = intensité de p
    iq = neigh.reshape((neigh.size,1)) # On reshape le voisinage en vecteur
    for i in range(iq.shape[0]):
        iq[i] = iq[i][0].getIntensity() # On récupère la valeur d'intensité de chaque pixel du voisinage
    mse = np.power((ip - iq),2).mean(axis=0) # On calcule l'erreur quadratique moyenne
    return 1/(1+math.sqrt(mse)) # on renvoie la valeur du poids correspondant au pixel p

def functionG(weight,x): # Fonction monote décroissante comprise entre 0 et 1
    return 1-(weight*x)/255 # 255 = maximum des intensités

class Automata:

    def __init__(self,img,seeds_fore,seeds_back): # Constructeur de l'automate
        self.listStates = [] # On crée une liste qui contiendra les différentes générations de l'automate
        tabStates = np.ndarray(img.shape,State) # On crée une matrice d'états (= génération 0) de la taille de l'image

        for i in range(tabStates.shape[0]): # On parcourt la matrice d'états
            for j in range(tabStates.shape[1]):
                tabStates[i,j] = State(img[i,j]) # On initialise chaque état avec l'intensité du pixel correspondant
        
        for state in tabStates[seeds_fore[:,0],seeds_fore[:,1]]: # Pour tous les pixels situés aux indices des seeds de premier plan
            state.setLabel(1) # On les labellise comme cellule cancéreuse
            state.setStrength(1) # On leur donne la force maximale

        for state in tabStates[seeds_back[:,0],seeds_back[:,1]]:
            state.setLabel(2) # On les labellise comme cellule saine
            state.setStrength(1) # On leur donne la force maximale

        self.listStates.append(tabStates) # On rajoute notre génération 0 à la liste des générations


    def evolve(self): # Règle d'évolution de l'automate
        t = self.listStates[-1] # On récupère la dernière génération créée
        t_1 = copy.deepcopy(self.listStates[-1]) # Au début de la règle, la génération à t+1 = la génération à t 
        for i in range(t.shape[0]): # On parcourt la génération t
            for j in range(t.shape[1]):
                p = t[i][j] # p est un pixel de la génération t, correspondant au pixel (i,j) de l'image
                neigh = t[i-1:i+2,j-1:j+2] # Voisinage de Moore d'ordre 1 du pixel p
                for i_q in range(neigh.shape[0]): # On parcourt le voisinage
                    for j_q in range(neigh.shape[1]):
                        q = neigh[i_q][j_q] # q est un pixel du voisinage du pixel p
                        w = weight(p, neigh) # On calcule le poids du pixel p
                        g = functionG(w,abs(p.getIntensity() - q.getIntensity())) # On calcule l'influence mutuelle de p et q
                        if (g*(q.strength) > p.strength): # Si la distance entre p et q ne permet pas d'atténuer suffisamment la force  de q
                            if (q.label != p.label): # Si le pixel p n'a pas encore été contaminé
                                t_1[i][j].setLabel(q.label) # On contamine le pixel p dans la génération à t+1
                            t_1[i][j].setStrength(g*(q.strength)) # On diminue la force du pixel contaminé
        self.listStates.append(t_1) # On rajoute la nouvelle génération créée à la liste des générations
                
        print('Génération', len(self.listStates) - 1, 'calculée')

    def image(self,n): # Renvoie une matrice de labels correspondant à la génération n
        img = np.ones(self.listStates[n].shape) # img est une image vide de la taille de la génération n
        for i in range(self.listStates[n].shape[0]): # On parcourt la génération n
            for j in range(self.listStates[n].shape[1]):
                img[i][j] = self.listStates[n][i][j].label # Chaque pixel de l'image prend la valeur du label correspondant
        return img # À la sortie, on a donc une image composée de 0 (pixels neutres), 1 (pixels contaminés) et 2 (pixels sains)



    
