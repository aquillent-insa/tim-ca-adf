import numpy as np 
import cv2 

class Extract:

    def __init__(self, img, color):

        # /!\ l'espace des couleurs d'OpenCV est BGR, et non RGB !!

        if color == 'blue': # Si on cherche à extraire la couleur bleue (seeds contaminés)
            lower_color_bounds = np.array([100,0,0], dtype='uint8')
            upper_color_bounds = np.array([255,80,80], dtype='uint8')
        
        elif color == 'red': # Si on cherche à extraire la couleur rouge (seeds sains)
            lower_color_bounds = np.array([0,0,100], dtype='uint8')
            upper_color_bounds = np.array([80,80,255], dtype='uint8')

        mask = cv2.inRange(img,lower_color_bounds,upper_color_bounds) # On récupère un masque des couleurs détectées
        # pixel du masque = 255 quand la couleur voulue est détectée dans le pixel correspondant
        self.indexes = np.array(np.where(mask==255)).T