class State:

    def __init__(self, valueIntensity): # Initialisation d'un état
        self.setLabel(0) # Label à 0
        self.setStrength(0) # Force à 0
        self.setIntensity(valueIntensity) # Intensité = intensité du pixel

    def setLabel(self, valueLabel): # Accesseur du label
        self.label = valueLabel

    def setStrength(self, valueStrength): # Accesseur de la force
        self.strength = valueStrength

    def setIntensity(self, valueIntensity): # Accesseur de l'intensité
        self.intensity = valueIntensity

    def getIntensity(self): # Getteur de l'intensité
        return int(self.intensity)
