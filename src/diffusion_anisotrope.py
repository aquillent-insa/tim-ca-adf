import numpy as np
import warnings

def diffusion_anisotrope(img,niter = 1, l = 20, dt = 0.1, option = 1):
    """
    Diffusion anisotrope de Perona et Malik.
    l est le lambda de la fonction de diffusivité.
    dt est le delta à appliquer aux différentes itérations.
    niter est le nombre d'itération à effectuer.
    """

    n,p = np.shape(img)
    img_out = img.astype(float).copy()


    for i in range(niter):
        img_temp = np.zeros([n+2,p+2])
        img_temp[1:n+1,1:p+1] = img_out

        # calcul des deltas
        delta_est = img_temp[1:n+1,0:p] - img_out 
        delta_ouest = img_temp[1:n+1,2:p+2] - img_out
        delta_nord = img_temp[0:n,1:p+1] - img_out
        delta_sud = img_temp[2:n+2,1:p+1] - img_out


        #conduction
        if (option == 1):
            grad_nord = np.exp(-(delta_nord/l)**2.)
            grad_sud = np.exp(-(delta_sud/l)**2.)
            grad_est = np.exp(-(delta_est/l)**2.)
            grad_ouest = np.exp(-(delta_ouest/l)**2.)
        else :
            grad_nord = 1./(1.+(delta_nord/l)**2.)
            grad_sud = 1./(1.+(delta_sud/l)**2.)
            grad_est = 1./(1.+(delta_est/l)**2.)    
            grad_ouest = 1./(1.+(delta_ouest/l)**2.)

        grad =  grad_nord * delta_nord + grad_sud * delta_sud + grad_est * delta_est + grad_ouest * delta_ouest 
        img_out += dt *grad
    
    return img_out


if __name__ == '__main__':

    import cv2
    import matplotlib.pyplot as plt
    fichier = 'banque_image/Colorectal.jpg'

    fichier = '../data/lena_gray.tif'

    img = cv2.imread(fichier,0)

    img1 = diffusion_anisotrope(img, niter = 10, l = 70, option = 1).astype('uint8')

    img2 = diffusion_anisotrope(img, niter = 20, l = 70, option = 1).astype('uint8')

    img3 = diffusion_anisotrope(img, niter = 50, l = 70, option = 1).astype('uint8')

    plt.ion()
    fig = plt.figure(frameon=True)

    ax1 = fig.add_subplot(131)
    ax1.imshow(img1, cmap='gray')
    ax1.set_title('10 Itérations')

    ax2 = fig.add_subplot(132)
    ax2.imshow(img2, cmap='gray')
    ax2.set_title('20 Itérations')

    ax3 = fig.add_subplot(133)
    ax3.imshow(img3, cmap='gray')
    ax3.set_title('50 Itérations')

    figManager = plt.get_current_fig_manager()
    figManager.full_screen_toggle()
    fig.canvas.toolbar.pack_forget()

    fig.savefig('../diapo/images/demo_70.jpg')


