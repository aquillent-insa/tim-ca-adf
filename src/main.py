import cv2
from Automata import Automata
from Extract import Extract
import numpy as np
import matplotlib.pyplot as plt
from diffusion_anisotrope import diffusion_anisotrope
import sys
import matplotlib.colors as colors

# Récupération des chemins des images et du nombre de générations via la ligne de commande
pathImg = sys.argv[1]
pathSeeds = sys.argv[2]
nbGenerations = int(sys.argv[3])

imgAvAni = cv2.imread(pathImg,0) # Chargement de l'image de la tumeur

nbIte = input('Nombre d\'itérations du filtre anisotrope (150 par défaut) : ')
lam = input('Valeur de lambda (20 par défaut) : ')

if (nbIte == ''):
    nbIte = '150'

if (lam == ''):
    lam = '20'

img = diffusion_anisotrope(imgAvAni,int(nbIte),int(lam)) # Passage de l'image dans un filtre anisotrope (200 itérations, lambda = 20)

seeds = cv2.imread(pathSeeds) # Chargement de l'image des seeds

ext_blue = Extract(seeds,'blue') # Récupération des indices des seeds de premier plan (couleur bleue)
ext_red = Extract(seeds,'red') # Récupération des indices des seeds d'arrière-plan (couleur rouge)

cmap = colors.ListedColormap(['grey', 'black', 'white']) # Création d'un espace de couleurs
# 0 <= gris < 1 <= noir < 2 <= blanc < 3
# Les pixels neutre sont en gris, les contaminés en noir et les sains en blanc

print('Initialisation')

auto = Automata(img, ext_blue.indexes, ext_red.indexes) # Création de l'automate à partir de l'image et des indices des seeds

#### AFFICHAGE ####

plt.ion() # Mode interactif de matplotlib
fig = plt.figure()
ax0 = fig.add_subplot(131) # Subplot de gauche
ax0.imshow(imgAvAni, cmap='gray') # Affichage de l'image de la tumeur
ax0.set_title('Image originale')
ax1 = fig.add_subplot(132) # Subplot du centre
ax1.imshow(img, cmap='gray') # Affichage de l'image post-filtre
ax1.set_title('Filtre anisotrope')
ax2 = fig.add_subplot(133) # Subplot de droite
img = ax2.imshow(seeds) # Affichage de l'image des seeds
ax2.set_title('Seeds entrés par l\'utilisateur')

plt.get_current_fig_manager().full_screen_toggle() # Passage en plein écran
fig.canvas.toolbar.pack_forget() # Masquage de la barre d'outils

fig.canvas.draw() # Affichage de l'interface

input('Appuyez sur <Entrée> pour passer à l\'étape suivante')

img = ax2.imshow(auto.image(0), cmap=cmap) # Affichage des seeds
ax2.set_title('Seeds compris par l\'automate')
fig.canvas.draw() # Mise à jour de l'interface

input('Appuyez sur <Entrée> pour lancer l\'automate')

for i in range(1,nbGenerations+1): # Pour chaque génération voulue
    auto.evolve() # On fait évoluer l'automate
    img = ax2.imshow(auto.image(i), cmap=cmap) # On affiche les labels de la génération courante
    s = 'Génération ' + str(i)
    ax2.set_title(s)
    fig.canvas.draw() # Mise à jour de l'interface

while (input('Appuyez sur <Entrée> pour quitter')):
    plt.show()

